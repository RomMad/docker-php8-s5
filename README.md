### Configuration Docker

Configuration Docker pour un environnement de développement PHP/Symfony :
- PHP 8
- Symfony 5

## TO DO

2.1. Check the current version of Docker :
```bash
docker -v
```
check the versioning [here](https://docs.docker.com/compose/compose-file/compose-versioning/).

2.2. create file "docker-compose.yml" in sub-folder "docker"

2.3. create a DockerFile in sub-folder 'php'

2.3. commands Docker

- Build image Docker :
```bash
docker-compose build
```

- Run Docker :
```bash
docker-compose up -d
```

- Stop Docker :
```bash
docker-compose down
```

2.4 entry in the shell ('bash') of container 'www'

```bash
docker exec -it www_docker_symfony bash
```

## Links

- phpmyadmin [here](http://127.0.0.1:8080/)
- maildev [here](http://127.0.0.1:8081/#/)

